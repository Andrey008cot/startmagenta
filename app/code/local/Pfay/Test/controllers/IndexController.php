<?php
class Pfay_Test_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function testAction ()
    {
//        $x=1;
//        while($x<50){
//            $this->countAction();
//        }
//        return true;

//        $retour = '';
//        $blogpost = Mage::getModel('test/test')->getCollection();
//
//        foreach($blogpost as $data){
//            $retour .= $data->getData('ip');
//        }
//        echo "<pre>";
//        var_dump($retour);
//        die('DIE');

    }
    public function countAction(){
        $model = Mage::getModel('test/test');
        $random = rand(100, 150);
        $ip = '192.168.1.' . $random;
        //if you want get
        //$ip = '$this->getRequest()->getParam('id')'
        $res = $model->getCollection()->addFilter('ip', $ip);
        $result=$res->getData('id');
        $id = $result[0]['id_pfay_test'];
        $views = $result[0]['views'];
        $views += 1;

        if($result){
            $this->updateAction($id, $views);
        }
        $this->insertAction($ip);

        $this->loadLayout();
        $this->renderLayout();
    }
    public function updateAction($id, $views){
        $model = Mage::getModel('test/test');
        $model->load($id);
        $model->setViews($views);
        $model->save();
        return true;
    }

    public function insertAction($ip){
        $model = Mage::getModel('test/test');
        $model->setViews('1');
        $model->setIp($ip);
        $model->save();
        return true;
    }
}