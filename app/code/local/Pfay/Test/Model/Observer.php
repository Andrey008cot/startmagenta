<?php
class Pfay_Test_Model_Observer {
    public function changePrice($observer){

        foreach( $observer->getCollection() as $product )
        {
            $product->setName ($product->getName() . 'Hello' );
            $product->setPrice ($product->getPrice() * 21 );
        }

    }
}